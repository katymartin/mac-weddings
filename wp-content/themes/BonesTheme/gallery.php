<?php
/*
 Template Name: Gallery
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
			
<?php get_header(); ?>

		<body>
			
<header>
<link href="https://fonts.googleapis.com/css?family=Great+Vibes|Playfair+Display|Raleway" rel="stylesheet">	
<meta name="viewport" content="width=device-width">

<div class="galHeader">
<div class="galSlog">
<h5>A photo lasts forever.</h5>
<h6>Gallery</h6>
</div>
<div class="photoCred">
<h3>PHOTOS COURTESY OF</h3>
<p><a href="http://www.kbattlephotography.com/" target="_blank">KBattle Photography</a> | <a href="http://jetaimeimages.blogspot.com/" target="_blank">Je t'aime Photography</a> | <a href="http://www.lottielillian.com/" target="_blank">Lottie Lillian Photography</a></p>
</div>


</div>



</header>





<section class="galleryWrap">

<div class="m-p-g">
	<div class="m-p-g__thumbs" data-google-image-layout data-max-height="350">
		<?php if( have_rows('gallery') ): ?>
			 <?php while( have_rows('gallery') ): the_row(); ?>
			 	<img src="<?php the_sub_field('gallery_photo'); ?>" data-full="<?php the_sub_field('gallery_photo'); ?>" class="m-p-g__thumbs-img" />
			<?php endwhile; ?>
 			<?php endif; ?>

	<div class="m-p-g__fullscreen"></div>
</div>

</div>
</section>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/45226/material-photo-gallery.min.js"></script>

<script>
	var elem = document.querySelector('.m-p-g');

	document.addEventListener('DOMContentLoaded', function() {
		var gallery = new MaterialPhotoGallery(elem);
	});
</script>







<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "Ch4Nm1mIz8kaIQAR";
//--></script>
<script type="text/javascript" src="https://rw1.calls.net/euinc/number-changer.js">
</script>
<!-- end ad widget -->	

	
			
</body>

<?php get_footer(); ?>
