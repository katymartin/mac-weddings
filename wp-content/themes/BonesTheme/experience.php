<?php
/*
 Template Name: Experience
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
			
<?php get_header(); ?>

			
<head>
<link href="https://fonts.googleapis.com/css?family=Great+Vibes|Playfair+Display|Raleway" rel="stylesheet">	
<meta name="viewport" content="width=device-width, initial-scale=1">

<div class="expHeader">
<div>
<h5>Every love story is beautiful.</h5>
<h6> The Experience </h6>
</div>
</div>



</head>

<body>
<section class="threeCol experience">

	<p>The Milwaukee Athletic Club offers a number of distinct and elegant spaces for your wedding alongside one of the best catering staffs in the city and a team of wedding consultants who can help you plan and manage the details of your special day. </p>
	<div class="threecolDiv expCopy">
		<div><h2>Your fairytale<br/>wedding begins here</h2><img src="/wp-content/themes/BonesTheme/library/images/accentHome.png"><h7>Full Service Venue</h7><p>Our venue spaces are perfect for ceremonies, cocktail hours, receptions, showers, and rehearsal dinners. With linens, chairs, tables, audio-visual, and set-up and tear down included, the MAC is the stress-free choice for your wedding events.</p></div>
		
		<div><h2>Once in<br/>a lifetime</h2><img src="/wp-content/themes/BonesTheme/library/images/accentHome.png"><h7>Customizable Menus</h7><p>Choose from a variety of passed appetizers, first courses, entr&eacute;es, and beverage services for your cocktail hour and reception. Our in-house catering kitchen and staff will assist you in creating a customizable menu for your big day</p></div>
		
		<div><h2>Keep your<br/>loved ones close</h2><img src="/wp-content/themes/BonesTheme/library/images/accentHome.png"><h7>Convenient Accomodations</h7><p>Our reasonably priced, full-service guest rooms are a great option for your most important guests. Every MAC wedding comes with a complimentary room for the bride and groom on the night of the wedding, and a complimentary room block for up to 20 rooms.</p></div>
		
	</div>
</section>
</body>
<?php get_footer(); ?>
