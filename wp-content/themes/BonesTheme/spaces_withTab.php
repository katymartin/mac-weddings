
			
<?php get_header(); ?>

			
<header>
<link href="https://fonts.googleapis.com/css?family=Great+Vibes|Playfair+Display|Raleway" rel="stylesheet">	

<meta name="viewport" content="width=device-width">

<div class="spacesHeader">
<div>
<h5>Leave room for magic.</h5>
<h6> VENUE SPACES </h6>
</div>
</div>

</header>
						
	
<section class="spaces clearfix">
<div class="tabGroup">	
	<ul class="tab">
  <li class="tabOdd"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Grand')" id="defaultOpen">the Grand Ballroom</a></li>
  
  <li ><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'roofTop')">the Rooftop Deck</a></li>
  
  
  
  
  </ul>
  <ul class="tab">
   <li class="tabOdd"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Superior')">the Superior Room</a></li>
  <li ><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Lounge')">the Lounge</a></li>
 
  
  
  </ul>
  <ul class="tab lastTab">
  <!-- <li><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Prez')">the Presidents Room</a></li> -->
  <li><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Elephant')">the Elephant Room</a></li>
  
  
   
</ul>
</div>

<div id="Grand" class="tabcontent">
<section class="venueInfo">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/grandballroom.jpg"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Contact')"><button>book a tour</button></a></div>
		<div class="venueCopy"><div><h1 class="slateH1">the Grand Ballroom</h1><h3>Classic Elegance</h3>
		<p>One of Milwaukee’s most luxurious ballrooms, The Grand Ballroom is a stunning venue replete with 20 foot columns, gold leaf ceilings, a balcony overlooking the room, and space for up to 300 guests and a large dance floor. <br/><br/>

		The Grand Ballroom can accommodate your ceremony, cocktail hour, and reception, or it can be rented alongside one of our other rooms for the full MAC experience. </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Hand-painted, 24k gold leaf ceilings</li>
					<li>+ High ceilings with 20 foot tall columns</li>
					<li>+ Massive crystal chandeliers</li>
					<li>+ Balcony overlooking the space</li>
					<li>+ Baby Grand Piano</li>
					<li>+ Large Dance Floor</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Space for up to 300 guests</li>
					<li>+ Packages ranging from $2,400 to $3,200</li>
					<li>+ Ivory or white table linens, napkins, chair covers and ties, and votive candles includeds</li>
					<li>+ Audio visual equipment and staging for band or DJ included</li>
					<li>+ Set-up and take down included</li>
			
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>
</div>

<div id="Superior" class="tabcontent">
 <section class="venueInfo">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/superior.jpg"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Contact')"><button>book a tour</button></a></div>
		<div class="venueCopy"><div><h1 class="slateH1">the Superior Room</h1><h3>Beautifully Dynamic</h3>
		<p>The Superior Room is a dynamic space with fantastic lighting, vaulted ceilings, and multiple floor styles that offers a separate entrance for the bridal party. Both formal and elegant, The Superior Room is a great fit for your ceremony or dinner reception, accommodating 250 for a ceremony and up to 150 for dinner.  </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Regal red carpet</li>
					<li>+ Hardwood center flood</li>
					<li>+ Brasschandeliers</li>
					<li>+ Balcony overlooking the space</li>
					<li>+ Upright Piano</li>
					
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Seats 250 for a ceremony</li>
					<li>+ Space for a 275-guest cocktail reception</li>
					<li>+ Fits 150 for a seated dinner or 100 with dance floor</li>
	
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>
</div>

<div id="roofTop" class="tabcontent">
  <section class="venueInfo">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/roofTop.jpg"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Contact')"><button>book a tour</button></a></div>
		<div class="venueCopy"><div><h1 class="slateH1">the Rooftop Deck</h1><h3>Breathtaking Views</h3>
		<p>The Deck at the MAC has quickly become one of the city’s most popular rooftop patios. The perfect space for outdoor events, our Deck offers a uniquely urban experience with immersive views of downtown Milwaukee and Lake Michigan that can accommodate a variety of reception or ceremony configurations, weather permitting. </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Rooftop patio</li>
					<li>+ Multiple areas and levels</li>
					<li>+ Well-lit for evening events</li>
					<li>+ City and lake views</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
				<li>+ Reception space for up to 225 people</li>
					<li>+ Ceremony space for up to 150 people</li>
					<li>+ Guaranteed indoor back-up space</li>
					<li>+ Rooftop bar with propane heaters</li>
					
					
			
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>
</div>	

<div id="Elephant" class="tabcontent">
  <section class="venueInfo">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/elephant.jpg"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Contact')"><button>book a tour</button></a></div>
		<div class="venueCopy"><div><h1 class="slateH1">the Elephant Room</h1><h3>Intimate Luxury</h3>
		<p>The Elephant Room is the Club’s premier bar space, offering an intimate and luxurious atmosphere that’s a perfect backdrop for a memorable cocktail reception. The Elephant Room features hand-painted elephant murals, animal print decor, and warm up-lighting, making it perfect for cozy cocktail receptions or private dinners.  </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Intimate bar setting</li>
					<li>+ Intricate elephant murals</li>
					<li>+ Animal print decor</li>
					<li>+ Ceiling up-lighting</li>
					<li>+ Baby Grand Piano</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Space for a 75-guest cocktail reception</li>
					<li>+ Intimate dinner space for 30 guests</li>
					<li>+ Banquette bench seating</li>
					<li>+ Built-in bar with animal print bar stools</li>
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>
<!--
</div>	
		
<div id="Prez" class="tabcontent">
  <section class="venueInfo">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/president.jpg"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Contact')"><button>book a tour</button></a></div>
		<div class="venueCopy"><div><h1 class="slateH1">the Presidents Room</h1><h3>Classic Elegance</h3>
		<p>The President’s Room is a warm, more casual setting that’s ideal for small cocktail receptions or ceremonies. The room features comfortable lounge furniture, a billiards table, and an upright piano, making it ideal for accommodating smaller wedding groups.  </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Billiards table</li>
					<li>+ Lounge furniture</li>
					<li>+ Upright piano</li>
					<li>+ Large windows</li>
			
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Seats 50 for a ceremony</li>
					<li>+ Space for a 75-guest cocktail reception</li>
					<li>+ Fits 50 for a seated dinner</li>
					<li>+ Banquette bench seating</li>		
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section> -->
</div>	
			
<div id="Lounge" class="tabcontent">
 <section class="venueInfo">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/lounge.jpg"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Contact')"><button>book a tour</button></a></div>
		<div class="venueCopy"><div><h1 class="slateH1">the Lounge</h1><h3>Classic Elegance</h3>
		<p>The Lounge offers a more intimate setting for up to 100 seated guests or 275 for a cocktail reception. With elegant hardwood flooring, large windows, an expansive faux fireplace, and an upright piano, the Lounge is a great space for your ceremony, cocktail hour, or even a small reception.</p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Parquet wood floors</li>
					<li>+ Large fireplace</li>
					<li>+ Upright piano</li>
					<li>+ Open and spacious</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Seats 225 for a ceremony</li>
					<li>+ Space for a 275-guest cocktail reception</li>
					<li>+ Fits 100 for seated dinner</li>
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>
</div>		
			
<div id="Contact" class="tabcontent">
  <section class="contact" id="booktour">	

<h1>Book a Tour</h1>
<div>
	<div class="floatLeft"><h4>Or Contact us Directly</h4><h4>414.274.0633<br/>weddings@macwi.org</h4>
	<p>The best way to see if MAC is right for your special day is to come visit us! One of our wedding consultants will give you a tour of our venues and discuss options and pricing. We encourage you to bring your future spouse, parents, family or friends. </p>
	</div>
	<div class="floatRight">
	<?php echo do_shortcode('[contact-form-7 id="14" title="Contact form 1"]'); ?>
	</div>
</div></section></div>					

</section>
<div>								
								
<?php get_footer(); ?>
</div>
