<?php
/*
 Template Name: Food and Bev
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
			
<?php get_header(); ?>


			
<head>
<link href="https://fonts.googleapis.com/css?family=Great+Vibes|Playfair+Display|Raleway" rel="stylesheet">	

<meta name="viewport" content="width=device-width, initial-scale=1">

<div class="menuHeader">
<div>
<h5>Eat, drink and be merry.</h5>
<h6> Food &amp; Beverage </h6>
</div>
</div>

</head>

<body>
<section class="foodBev">
	<!--
		<div class="foodBevCopy">
		<div><img src="/wp-content/themes/BonesTheme/library/images/food.png"><h2>Food</h2><p class="borderp">Choose from a variety of freshly prepared passed hors d'oeuvres like bruschetta and shrimp shooters, an assortment of salads and breads for your first course, and over a dozen entrees and sides like roasted salmon with sweet potato hash and angus beef tenderloin with seasonal vegetables. <br/><br/>

		We’re happy to accommodate your guests’ dietary restrictions and also offer a number of kid-friendly meals for guests 10 and under. We also offer a number of late night selections like house made pizzas and sliders for the last guests standing. </p></div>
		
		
		<div><img src="/wp-content/themes/BonesTheme/library/images/bev.png"><h2>Beverage</h2><p>We offer 3 hours of consecutive beverage service from your cocktail hour through your reception. Choose from an assortment of call and premium liquors, wines, and beers (including local favorites) at price points tailored to your budget. <br/><br/>

		We also offer a comprehensive selection of non-alcoholic beverages and an optional champagne toast for all guests. Unfortunately, Wisconsin law prohibits guest from bringing their own alcoholic beverages into the venue. We do, however, allow you to fully customize the beverage service at your wedding for an additional charge.</p></div>
		
	</div>
	
	-->
	
	<div class="menuDownload"><a href="https://drive.google.com/file/d/0B3TJK0xa6nGpTnZrTDdaZ2d6QmQ5SFhLNXgydGsxNzNoZHJR/view?usp=sharing" target="_blank"><button>download full food &amp; beverage menu </button></a></div>
</section>
						
											
							
								
								
								
								
								
								
								
								
								
								
						<footer class="article-footer cf">
									


								</footer>

							
<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "Ch4Nm1mIz8kaIQAR";
//--></script>
<script type="text/javascript" src="https://rw1.calls.net/euinc/number-changer.js">
</script>
<!-- end ad widget -->	

					

	</body>		



<?php get_footer(); ?>
