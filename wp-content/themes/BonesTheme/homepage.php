<?php
/******
 Template Name: Homepage
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>



<head>




<meta name="viewport" content="width=device-width, initial-scale=1">



<div class=" homeHeader">
 <div class="homeHeadContent">
<img src="/wp-content/themes/BonesTheme/library/images/homeBannerimg.png">
	<div class="bannerCopy" alt="Milwaukee Athletic Club Weddings. Classic. Elegant. Timeless.">

	</div>

</div>
</div>

<section class="mainCopy">
	<div>
		<p>The Milwaukee Athletic Club is a city club rich in history, luxury, and elegance. Here at the MAC, we pride ourselves on exquisite and regal event spaces, offering your guests a first class fine dining experience with exceptional service. Our in-house Wedding Planning Team will guide you seamlessly every step of the way to your dream wedding.</p>
	</div>
	<div class="tourButton">
		<a href="#booktour " target="_self" ><h4>Book a Tour</h4></a>
	</div>
</section>


</head>

<body>

<section class="threeCol venueFeature">
	<h1>Featured Spaces</h1>
	<div class="threecolDiv venuesHome">
		<div>
			<a href="/spaces/#ballroom" target="_self"><img src="/wp-content/themes/BonesTheme/library/images/grandballroom.jpg" alt="Grand Ballroom Aearial Shot"></a>
			<h2>the Grand Ballroom</h2>
			<h3>Classic Elegance</h3>
			<a href="/spaces/#ballroom" target="_self"><button>learn more</button></a>
		</div>
		<div>
			<a href="/spaces/#rooftop" target="_self"><img src="/wp-content/themes/BonesTheme/library/images/roofTop.jpg" alt="Summer Time open air rooftop deck"></a>
			<h2>the Rooftop Deck</h2>
			<h3>Breathtaking Views</h3>
			<a href="/spaces/#rooftop" target="_self"><button>learn more</button></a>
		</div>
		<div>
			<a href="/spaces/#superior" target="_self"><img src="/wp-content/themes/BonesTheme/library/images/superiorhome.jpg" alt="Winter decor Superior Room"></a>
			<h2>the Superior Room</h2>
			<h3>Beautifully Dynamic</h3>
			<a href="/spaces/#superior" target="_self"><button>learn more</button></a>
		</div>

	</div>

</section>

<section class="threeCol planning">
<h1>Planning</h1>
	<div class="threecolDiv planningDiv">
		<div class="planningItem">
		<img src="/wp-content/themes/BonesTheme/library/images/step1.png" alt="step 1"/>
		<h3>Join us<br/>for a Tour</h3>
		<p>We invite you to join us for a tour of the Milwaukee Athletic Club. The Grand Ballroom will take your breath away with original Swarovski Crystal Chandeliers, and a 24k gold leaf hand-painted ceiling. Be sure to view the Rooftop Deck, one of the most breathtaking views of the downtown Milwaukee skyline and Lake Michigan, and boasting beautiful Edison string lights as a spectacular backdrop for your photos.</p>
		</div>

		<div class="planningItem">
		<img src="/wp-content/themes/BonesTheme/library/images/step2.png" alt="step 2"/>
		<h3>Set<br/>the Date</h3>
		<p>Congratulations on selecting your gorgeous wedding venue! After booking, enjoy a private menu tasting from our incredible Executive Chef. The MAC's Wedding Planning Team will assist you in selecting your linens, drafting your floor plan, and designing the menu that all of your guests will be talking about for years to come.</p>
		</div>

		<div class="planningItem">
		<img src="/wp-content/themes/BonesTheme/library/images/step3.png" alt="step 3"/>
		<h3>Enjoy Your<br/>Dream Wedding</h3>
		<p>Sit back, relax, and revel in the love of your family and friends as our MAC Wedding Planning Team assists you every step of the way. Listen to the gasps and awe as your guests enter the Grand Ballroom and experience elegance at its best. Enjoy a first class fine dining dinner, as our exceptional service staff caters to your every need. Cherish the memories the Milwaukee Athletic Club brings to life.</p>
		</div>

	</div>
	<div class="tourButton">
		<a href="#booktour" target="_self"><h4>Book a Tour</h4></a>
	</div>
</section>

<section class="testimonialsFlick">
<div class="main-gallery">
  <div class="gallery-cell">
    <div class="testimonial">
        <img class="testimonial-avatar" src="/wp-content/themes/BonesTheme/library/images/quote.png">
      <q class="testimonial-quote">"Our wedding at the Milwaukee Athletic Club was all around awesome!!! We were truly in the best hands at the MAC. The ballroom is absolutely beautiful. We did not have to bring in much decor because the room really spoke for itself. The service was excellent. The food was absolutely DELICIOUS (try items from the new menu). They were so incredibly accommodating for all of our guests. Overall, the MAC was amazing. They made the entire process totally effortless!!!!"</q>
      <span class="testimonial-author">Theresa W.</span>
    </div>
  </div>
  <div class="gallery-cell">
     <div class="testimonial">
        <img class="testimonial-avatar" src="/wp-content/themes/BonesTheme/library/images/quote.png">
      <q class="testimonial-quote">"What an amazing experience I had working with the MAC! While planning our MKE wedding from ATL, I found [the staff] to be so communicative and responsive and helpful with any and all questions I had. We had our full event there, ceremony, cocktails and reception. The compliments I received from our guests were immediate and are still coming. The food was WONDERFUL, the value was indescribable. I honestly cannot say enough good things about this venue. The food, ambiance, the treatment our guests received upon arrival... and the extras they were willing to help us with were truly top-notch. A huge thank you to the staff of the MAC for making our event completely unforgettable."</q>
      <span class="testimonial-author">Elizabeth</span>
    </div>
  </div>
  <div class="gallery-cell">
        <div class="testimonial">
        <img class="testimonial-avatar" src="/wp-content/themes/BonesTheme/library/images/quote.png">
      <q class="testimonial-quote">"The staff and facilities are great! We were able to use the Rooftop Deck for cocktail hour being a summer wedding. The Grand Ballroom was amazing and the food was excellent. I would highly recommend this as a downtown Milwaukee wedding venue. They also have hotel rooms available for out of town guests!"</q>
      <span class="testimonial-author">Amanda P.</span>
    </div>
  </div>
</div>
</section>

<!--
<section class="testimonials">
<div class="threeCol">
<h1>Testimonials</h1>
	<div class="threecolDiv testimonialsDiv">
		<div class="testItem">
		<img src="/wp-content/themes/BonesTheme/library/images/quote.png" alt="quote"/>
		<p>Our wedding at the Milwaukee Athletic Club was all around awesome!!! We were truly in the best hands at the MAC. The ballroom is absolutely beautiful. We did not have to bring in much decor because the room really spoke for itself. The service was excellent. The food was absolutely DELICIOUS (try items from the new menu). They were so incredibly accommodating for all of our guests. Overall, the MAC was amazing. They made the entire process totally effortless!!!!</p>
		<h3>-Theresa W.</h3>
		</div>

		<div>
		<img src="/wp-content/themes/BonesTheme/library/images/quote.png" alt="quote"/>
		<p>What an amazing experience I had working with the MAC! While planning our MKE wedding from ATL, I found [the staff] to be so communicative and responsive and helpful with any and all questions I had. We had our full event there, ceremony, cocktails and reception. The compliments I received from our guests were immediate and are still coming. The food was WONDERFUL, the value was indescribable. I honestly cannot say enough good things about this venue. The food, ambiance, the treatment our guests received upon arrival... and the extras they were willing to help us with were truly top-notch. A huge thank you to the staff of the MAC for making our event completely unforgettable.</p>
		<h3>-Elizabeth</h3>
		</div>

		<div>
		<img src="/wp-content/themes/BonesTheme/library/images/quote.png" alt="quote"/>
		<p>The staff and facilities are great! We were able to use the Rooftop Deck for cocktail hour being a summer wedding. The Grand Ballroom was amazing and the food was excellent. I would highly recommend this as a downtown Milwaukee wedding venue. They also have hotel rooms available for out of town guests!</p>
		<h3>-Amanda P.</h3>
		</div>

	</div>
</div>
</section>
-->

<?php include 'contactForm.php';?>




<script src='https://cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.pkgd.min.js'></script>





						<footer class="article-footer cf">



								</footer>

<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "Ch4Nm1mIz8kaIQAR";
//--></script>
<script type="text/javascript" src="https://rw1.calls.net/euinc/number-changer.js">
</script>
<!-- end ad widget -->

<script>
var flkty = new Flickity( '.main-gallery', {
  cellAlign: 'left',
  contain: true,
  wrapAround: true,
  prevNextButtons: false,
  autoPlay: 5000
});

</script>

</body>







<?php get_footer(); ?>
