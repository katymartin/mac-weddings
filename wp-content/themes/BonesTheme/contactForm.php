<!DOCTYPE html>

<style>

.nf-form-fields-required {display: none !important;}

.nf-field-element input, .nf-field-element select, .nf-field-element textarea {
	padding: 10px !important;
	margin: 10px 0px !important;
}

.field-wrap input[type="submit"], .field-wrap input[type="button"], .field-wrap button {
	    width: 123px !important;
    margin: auto !important;
    display: block !important;
}
.nf-field-element textarea {
	height: 140px !important;
}
	}	
</style>


<section class="contactSection" id="booktour">	
<div class="contact">

<div class="contactBox clearfix">
<h1>Book a Tour</h1>
<div>
	<div class="floatLeft"><h4>Or Contact Us Directly</h4><h4>414.274.0633<br/>weddings@macwi.org</h4>
	<p>The best way to see if MAC is right for your special day is to come visit us! One of our wedding consultants will give you a tour of our venues and discuss options and pricing. We encourage you to bring your future spouse, parents, family or friends. </p>
	</div>
	<div class="floatRight">
	<?php echo do_shortcode('[ninja_form id=2]'); ?>
	</div>
</div>
</div>					
</div>					
</section>	