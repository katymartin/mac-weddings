<?php
/*
 Template Name: Thank You
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('track', 'CompleteRegistration');
</script>

<!-- Google Code for Book a Tour Form Submitted Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 841004742;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "vGPECJannHUQxu2CkQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/841004742/?label=vGPECJannHUQxu2CkQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<head>
<style>

/*********************
THANK YOU PAGE
*********************/

.thankYou {
	width: 100%;
	height: 80%;
	background-color: #f9f3ed;
	text-align: center;
	padding: 10% 20px;
}
.thankYou h1 {
		font-size: 12vw;
		margin: 0;
	}
.thankYou h3 {
		font-size: 3vw;
	}
</style>



</head>

<body>

<section class="thankYou">
	<h1>Thank You!</h1>
	<h3>We'll be in touch soon!</h3>
</section>



						<footer class="article-footer cf">



								</footer>

	<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "Ch4Nm1mIz8kaIQAR";
//--></script>
<script type="text/javascript" src="https://rw1.calls.net/euinc/number-changer.js">
</script>
<!-- end ad widget -->

	</body>






<?php get_footer(); ?>
