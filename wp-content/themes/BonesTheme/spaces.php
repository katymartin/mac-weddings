<?php
/*
 Template Name: Spaces
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>
			
<?php get_header(); ?>

	
<head>
<link href="https://fonts.googleapis.com/css?family=Great+Vibes|Playfair+Display|Raleway" rel="stylesheet">	

<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="spacesHeader">
<div>
<h5>Leave room for magic.</h5>
<h6> VENUE SPACES </h6>
</div>
</div>

</head>

<body>							
	
<section class="spaces clearfix">

<!--
<div class="tabGroup">	
 <ul>
  <li><a href="#ballroom" target="_self">the Grand Ballroom</a></li>
  <li><a href="#rooftop" target="_self">the Rooftop Deck</a></li>
  <li><a href="#superior" target="_self">the Superior Room</a></li>
  <li><a href="#lounge" target="_self">the Lounge</a></li>
  <li><a href="#elephant" target="_self">the Elephant Room</a></li>
</ul>
</div>
-->

<section id="ballroom" class="venueInfo clearfix">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/grandballroom.jpg"></div>
		<div class="venueCopy"><div><h2>the Grand Ballroom</h2><h3>Classic Elegance</h3>
		<p>One of Milwaukee’s most luxurious ballrooms, The Grand Ballroom is a stunning venue replete with 20 foot columns, gold leaf ceilings, a balcony overlooking the room, and space for up to 300 guests and a large dance floor. <br/><br/>

		The Grand Ballroom can accommodate your ceremony, cocktail hour, and reception, or it can be rented alongside one of our other rooms for the full MAC experience. </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Hand-painted, 24k gold leaf ceilings</li>
					<li>+ Lofted ballroom with 20 foot tall columns</li>
					<li>+ Massive crystal chandeliers</li>
					<li>+ Balcony overlooking the space</li>
					<li>+ Baby grand piano</li>
					<li>+ Large hardwood dance floor</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Space for up to 300 guests</li>
					<li>+ Table linens, napkins, chair covers, and votive candles included</li>
					<li>+ Audio visual equipment and staging for band or DJ included</li>
					<li>+ Set up and tear down included</li>
			
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>


<section id="rooftop" class="venueInfo clearfix">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/roofTop.jpg"></div>
		<div class="venueCopy"><div><h2>the Rooftop Deck</h2><h3>Breathtaking Views</h3>
		<p>The Deck at the MAC has quickly become one of the city’s most popular rooftop patios. The perfect space for outdoor events, our Deck offers a uniquely urban experience with breathtaking views of downtown Milwaukee and Lake Michigan. </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Rooftop patio</li>
					<li>+ Multiple areas and levels</li>
					<li>+ Well-lit for evening events</li>
					<li>+ City and lake views</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Reception space for up to 225</li>
					<li>+ Ceremony space for up to 150</li>
					<li>+ Guaranteed indoor back-up space</li>
					<li>+ Large built-in bar</li>
					<li>+ Portable propane heaters</li>
					
					
			
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>

 <section id="superior" class="venueInfo clearfix">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/superiorhome.jpg"></div>
		<div class="venueCopy"><div><h2>the Superior Room</h2><h3>Beautifully Dynamic</h3>
		<p>The Superior Room is a dynamic space that is both formal and elegant. Boasting fantastic lighting, and vaulted ceilings. This is our most popular ceremony space with brass chandeliers and colonial gold curtains.</p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Regal red carpet</li>
					<li>+ Centered hardwood dance floor</li>
					<li>+ Brass chandeliers</li>
					<li>+ Upright piano</li>
					
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Ceremony seating for up to 250</li>
					<li>+ Cockatail reception space for up to 275</li>
					<li>+ Seated dinner for up to 150</li>
	
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>


 <section id="lounge" class="venueInfo clearfix">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/lounge.jpg"></div>
		<div class="venueCopy"><div><h2>the Lounge</h2><h3>Classic Elegance</h3>
		<p>With elegant hardwood flooring, large windows, an expansive faux fireplace, and an upright piano, the Lounge is a great space for your ceremony, cocktail hour, or even a small reception.</p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Parquet wood floors</li>
					<li>+ Large fireplace</li>
					<li>+ Upright piano</li>
					<li>+ Open and spacious</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Ceremony space for up to 225</li>
					<li>+ Cocktail receptin space for up to 275</li>
					<li>+ Seated dinner for up to 100</li>
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>
 
 
  <section id="elephant" class="venueInfo clearfix">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/elephant.jpg"></div>
		<div class="venueCopy"><div><h2>the Elephant Room</h2><h3>Intimate Luxury</h3>
		<p>The Elephant Room is the Club’s premier bar space, offering an intimate and luxurious atmosphere that’s a perfect backdrop for a memorable cocktail reception. The Elephant Room features hand-painted elephant murals and animal print decor, making it perfect for cozy cocktail receptions or private dinners.</p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Intimate bar setting</li>
					<li>+ Intricate elephant murals</li>
					<li>+ Animal print decor</li>
					<li>+ Ceiling uplighting</li>
					<li>+ Baby grand piano</li>
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Cocktail reception space for up to 175</li>
					<li>+ Intimate dinner space for 30</li>
					<li>+ Banquette bench seating</li>
					<li>+ Built-in bar with animal print bar stools</li>
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section>
<!--
</div>	
		
<div id="Prez" class="tabcontent">
  <section class="venueInfo">
	<div>
		<div class="venueImg"><img src="/wp-content/themes/BonesTheme/library/images/president.jpg"><a href="javascript:void(0)" class="tablinks" onclick="openSpace(event, 'Contact')"><button>book a tour</button></a></div>
		<div class="venueCopy"><div><h1 class="slateH1">the Presidents Room</h1><h3>Classic Elegance</h3>
		<p>The President’s Room is a warm, more casual setting that’s ideal for small cocktail receptions or ceremonies. The room features comfortable lounge furniture, a billiards table, and an upright piano, making it ideal for accommodating smaller wedding groups.  </p></div>
		<div class="bullets">
			<div class="atmo">
			<h7>the Atmosphere</h7>
				<ul>
					<li>+ Billiards table</li>
					<li>+ Lounge furniture</li>
					<li>+ Upright piano</li>
					<li>+ Large windows</li>
			
				</ul>
			</div>
			<div>
			<div class="details">
			<h7>the Details</h7>
				<ul>
					<li>+ Seats 50 for a ceremony</li>
					<li>+ Space for a 75-guest cocktail reception</li>
					<li>+ Fits 50 for a seated dinner</li>
					<li>+ Banquette bench seating</li>		
				</ul>
			</div>
			<div>
			</div>
			
		</div>
		
		</div>
	</div>
</section> -->

			


	</section>
			
<?php include 'contactForm.php';?>								
	<!-- start number replacer -->
<script type="text/javascript"><!--
vs_account_id      = "Ch4Nm1mIz8kaIQAR";
//--></script>
<script type="text/javascript" src="https://rw1.calls.net/euinc/number-changer.js">
</script>
<!-- end ad widget -->
	</body>							
<?php get_footer(); ?>

