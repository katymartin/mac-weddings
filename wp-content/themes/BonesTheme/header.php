<!doctype html>



<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->

<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->

<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->

<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->


<meta name="google-site-verification" content="h2-YxcYDc1XWlY_H0ul8W6zZoP78_i6mrs7U-woM5hU" />


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '305724183215688'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=305724183215688&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Code for Wedding Venue Inquiry Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 845707994;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "ZjOaCNeey3MQ2vWhkwM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/845707994/?label=ZjOaCNeey3MQ2vWhkwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
	
<!-- End Google Ad Word Code -->	
<script>
 
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
 ga('create', 'UA-39015905-21', 'auto');
 ga('create', 'UA-78694691-44', 'auto', 'ppcTracker');
 
 ga('send', 'pageview');
 ga('ppcTracker.send', 'pageview');
 
</script>



		<meta charset="utf-8">

<meta name="google-site-verification" content="h2-YxcYDc1XWlY_H0ul8W6zZoP78_i6mrs7U-woM5hU" />


		<?php // force Internet Explorer to use the latest rendering engine available ?>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
	<link href="https://fonts.googleapis.com/css?family=Great+Vibes|Playfair+Display|Raleway" rel="stylesheet">	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.min.css'>

  

		<title><?php wp_title(''); ?></title>

	

		<?php wp_head(); ?>
	<head>
	

<div class="preHeader">
	<ul>
		<li><a href="tel:4142740633"><i class="fa fa-phone" aria-hidden="true"></i>414.274.0633</a></li>
		<li><a href="mailto:weddings@macwi.org"><i class="fa fa-envelope-o" aria-hidden="true"></i>weddings@macwi.org</a></li>
	</ul>
</div>		

<section class="KTheader">

<a href="/homepage/" target="_self"><img class="deskLogo" src="/wp-content/themes/BonesTheme/library/images/MAClogo.png"></a>



<ul class="menu">

		<li><a href="/the-experience/">The Experience</a></li>

		<li><a href="/spaces/">Venue Spaces</a></li>
		
		<li><a href="/gallery/">Gallery</a></li>

		<li><a href="/food-bev/">Food &amp; Beverage</a></li>
		<!--
		<li><a href="/team/">Wedding Team</a></li>
		-->
		<li class="bookTour"><a href="/homepage/#booktour"><h4>Book a Tour</h4></a></li>

	</ul>

</section>

	

	

	

	<nav class="mobile">

   <a href="/homepage/" target="_self"><img class="mobileLogo" src="/wp-content/themes/BonesTheme/library/images/MAClogo.png"></a>

   

    <div id="nav-trigger">

      <i class="fa fa-bars"></i>

    </div>

    <ul class="nav-content">

	<li><a href="/the-experience/">The Experience</a></li>

		<li><a href="/spaces/">Venue Spaces</a></li>
		
		<li><a href="/gallery/">Gallery</a></li>

		<li><a href="/food-bev/">Food &amp; Beverage</a></li>
<!--
		<li><a href="/team/">Wedding Team</a></li>
-->
		<li class="bookTour"><a href="/homepage/#booktour"><h4>Book a Tour</h4></a></li>

     </ul>



  </nav>

	

	<style>

.homeHeader{

position:inherit !important;

}

</style>



 

		



	</head>



	

